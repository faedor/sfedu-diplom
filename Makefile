venv:
	virtualenv -p python3.5 venv

requirements:
	pip install -r requirements.txt

designer:
	/usr/lib/x86_64-linux-gnu/qt5/bin/designer

form:
	pyuic5 src/ui/main_form.ui > src/ui/main_form.py

run:
	python src/main.py

build:
	make form
	make run
