# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'src/ui/main_form.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(847, 389)
        self.startButton = QtWidgets.QPushButton(Form)
        self.startButton.setGeometry(QtCore.QRect(100, 190, 121, 27))
        self.startButton.setCheckable(False)
        self.startButton.setDefault(False)
        self.startButton.setFlat(False)
        self.startButton.setObjectName("startButton")
        self.logBox = QtWidgets.QTextEdit(Form)
        self.logBox.setGeometry(QtCore.QRect(330, 20, 501, 351))
        self.logBox.setReadOnly(False)
        self.logBox.setObjectName("logBox")
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(40, 30, 281, 31))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setGeometry(QtCore.QRect(40, 80, 131, 17))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Form)
        self.label_3.setGeometry(QtCore.QRect(40, 120, 141, 17))
        self.label_3.setObjectName("label_3")
        self.stepOptimizationBox = QtWidgets.QDoubleSpinBox(Form)
        self.stepOptimizationBox.setGeometry(QtCore.QRect(210, 30, 62, 27))
        self.stepOptimizationBox.setProperty("value", 15.0)
        self.stepOptimizationBox.setObjectName("stepOptimizationBox")
        self.optimizationBox = QtWidgets.QDoubleSpinBox(Form)
        self.optimizationBox.setGeometry(QtCore.QRect(210, 70, 62, 27))
        self.optimizationBox.setProperty("value", 50.0)
        self.optimizationBox.setObjectName("optimizationBox")
        self.maxStepsBox = QtWidgets.QDoubleSpinBox(Form)
        self.maxStepsBox.setGeometry(QtCore.QRect(210, 110, 62, 27))
        self.maxStepsBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.maxStepsBox.setDecimals(0)
        self.maxStepsBox.setProperty("value", 10.0)
        self.maxStepsBox.setObjectName("maxStepsBox")
        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(40, 150, 261, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.startButton.setText(_translate("Form", "Старт"))
        self.label.setText(_translate("Form", "<html><head/><body><p>удаляемое на каждом <br/>шаге кол-во элементов</p></body></html>"))
        self.label_2.setText(_translate("Form", "цель оптимизации"))
        self.label_3.setText(_translate("Form", "max число шагов"))

