"""
Пользовательские слоты для виджетов.
"""
# Импортируем модуль времени
from datetime import datetime
# Импортируем класс интерфейса из созданного конвертером модуля
from ui.main_form import Ui_Form

# Создаём собственный класс, наследуясь от автоматически сгенерированного
from utils.main import Problem

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication

class MainWindowSlots(Ui_Form):
    # пользовательский слот

    step_optimization = None
    optimization = None
    max_steps = None

    def log(self, message):
        self.logBox.append(message)

    def validate_form(self):
        if (self.step_optimization == 0) or \
                (self.optimization == 0) or (self.max_steps == 0):
            log_message = "Заполните все поля"
            self.log(log_message)
            QApplication.restoreOverrideCursor()
        else:
            log_message = "Пожалуйста, подождите..."
            self.logBox.clear()
            self.log(log_message)
            self.progressBar.setValue(1)
            self.run_optimization()

    def run_optimization(self):

        log_message = """
        Исходные данные:
            шаг оптимизации: {step_opt}
            цель оптимизации: {opt}
            максимальное кол-во шагов: {max_steps}""".format(
            step_opt=self.step_optimization, opt=self.optimization,
            max_steps=self.max_steps)

        self.log(log_message)

        initial_data = {
            'form': self,
            'problem_id': 32,
            'step_optimization': self.step_optimization,
            'optimization': self.optimization,
            'max_steps': self.max_steps
        }
        step = 0
        solver = Problem(**initial_data)
        solver.solve()
        if solver.data:
            while not (solver._solution_success(step)):
                solver._log("-> итерация № {}".format(step))
                solver._get_intermediate_solution()
                if solver.intermediate_solution:
                    solver._find_and_remove_items()
                    step += 1
                    self.progressBar.setValue(100 / self.max_steps * step)
                else:
                    break
        self.progressBar.setValue(100)
        QApplication.restoreOverrideCursor()

    def start_optimize(self):
        QApplication.setOverrideCursor(Qt.WaitCursor)
        self.step_optimization = self.stepOptimizationBox.value()
        self.optimization = self.optimizationBox.value()
        self.max_steps = int(self.maxStepsBox.value())
        self.validate_form()