import logging
import requests
import sys

from operator import itemgetter
from fake_useragent import UserAgent

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%d.%m.%Y %H:%M',
                    filename='logs.log')
                    #filemode='w')

logging.getLogger().addHandler(logging.StreamHandler())

DEBUG = False
TEST_MODE = False

TEST_INITIAL_DATA = [
    {"id": 1, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 2, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 3, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 4, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 5, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 6, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 7, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 8, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 9, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True},
    {"id": 10, "x": 0, "y": 0, "z": 0, "value": 0, "removable": True}
]

TEST_SOLVE_RESULT = [
    {'id': 1, 'value': 0.5},
    {'id': 2, 'value': 0.1},
    {'id': 3, 'value': 0.2},
    {'id': 4, 'value': 0.3},
    {'id': 5, 'value': 0.4},
    {'id': 6, 'value': 0.5},
    {'id': 7, 'value': 0.6},
    {'id': 8, 'value': 0.7},
    {'id': 9, 'value': 0.8},
    {'id': 10, 'value': 0.5},
]


def get_value_by_percent(hundred_percent_value, percent):
    """
    Получение величины по проценту
    :param hundred_percent_value: величина, соответствующая 100%
    :param percent: процент для определения фактического значения
    :return:
    """
    return int(round(hundred_percent_value * percent / 100))


class Problem:
    """
    Класс для решения задачи
    """

    PROBLEM_URLS_TPLS = {
        'list': 'http://acelan.ru/api/optimization/problems/',
        'details': 'http://acelan.ru/api/optimization/problems/{problem_id}/'
    }

    TIMEOUT = 5
    MAX_TRY = 4

    def __init__(self, form, problem_id, step_optimization, optimization, max_steps):
        """
        Инициализация класса
        :param form: UI форма
        :param step_optimization: кол-во удаляемых элементов за шаг (%)
        :param optimization: цель оптимизации (%)
        :param max_steps: максимально допустимое количество шагов
        """
        self.form = form
        self.problem_id = problem_id
        self.step_optimization = step_optimization
        self.optimization = optimization
        self.max_steps = max_steps
        self.data = None
        self.intermediate_solution = None
        self.count_to_remove = 0
        self.optimization_count = 0
        self.removed_count = 0  # здесь накпливается кол-во удаленных элементов
        self.urls = {
            'list': self.PROBLEM_URLS_TPLS['list'],
            'details': self.PROBLEM_URLS_TPLS['details'].format(
                problem_id=self.problem_id)
        }
        self.user_agent = UserAgent()

    def _log(self, message):
        """
        Логирование сообщений
        :param message: сообщение
        :return:
        """
        logging.info(message)  # запись в стандартный лог
        if self.form:
            self.form.log(message)  # запись в лог формы

    @property
    def _request_settings(self):
        """
        Settings to make requests with random user agent
        :return: dict with settings
        """
        return {
            'timeout': self.TIMEOUT,
            'headers': {'User-Agent': '{}'.format(self.user_agent.random)},
            'verify': False,
        }

    def _get_server_response(self, method, data=None):
        res = None
        current_try = 1
        while True:
            try:
                res = requests.request(method=method, url=self.urls['details'],
                                       data=data,
                                       **self._request_settings).json()
            except Exception as e:
                self._log("Cannot get response: {}".format(str(e)))
            if res or (current_try == self.MAX_TRY):
                break
            current_try += 1
        return res

    def _get_initial_data(self):
        """
        Получение исходных данных задачи
        :return:
        """

        data_length = len(self.data)

        # цель оптимизации - сколько элементов необходимо удалить для решения
        # задачи
        self.optimization_count = get_value_by_percent(
            hundred_percent_value=data_length, percent=self.optimization)
        self._log("Цель оптимизации: {} элементов к удалению".format(
            self.optimization_count))

        # кол-во удаляемых элементов на каждом шаге
        self.count_to_remove = get_value_by_percent(
            hundred_percent_value=data_length, percent=self.step_optimization)
        self._log("Кол-во удаляемых элементов на каждом шаге: {}".format(
            self.count_to_remove))

        if DEBUG:
            print('Initial data: {}\nCount to remove: {}'.format(
                self.data, self.count_to_remove))

    def _reformat_initial_data(self):
        self._log("реформатирование исходных данных")
        items = [
            {
                'id': item['id'],
                'remove': False
            } for item in self.data
        ]
        self.data = {'items': items, 'final': False}

        if DEBUG:
            print('Reformatted initial data: {}'.format(self.data))

    def _find_and_remove_items(self):
        self._log("поиск и удаление элементов")
        sorted_intermediate_solution = sorted(self.intermediate_solution,
                                              key=itemgetter('value'))[
                                       :self.count_to_remove]
        sorted_ids = [item['id'] for item in sorted_intermediate_solution]

        for item in self.data['items']:
            item['remove'] = item['id'] in sorted_ids

        self.removed_count += len(sorted_ids)

    def _get_intermediate_solution(self):
        # отправляем запрос с данными для получения сиписка элементов для
        # следующего шага решения
        # FIXME: если отправлять self.data, то приходит код 500
        # ,  data=self.data

        self._log("запрос на получение промежуточного решения")
        self.intermediate_solution = self._get_server_response('POST') #data=TEST_INITIAL_DATA)

    def _solution_success(self, step):
        self._log("проверка окончания решения")
        solved = (step >= self.max_steps) or (
            self.removed_count >= self.optimization_count)
        self._log("задача решена: {}".format(solved))
        return solved

    def solve(self):
        self._log("Запуск процесса решения задачи")
        if DEBUG:
            print('Solving start...')
        self._log("Отправка запроса для получения исходных данных")
        self.data = self._get_server_response("GET")
        if self.data:
            self._get_initial_data()
            self._reformat_initial_data()

if __name__ == '__main__':
    # TODO: use real settings

    settings = {
        'form': None,
        'problem_id': 32,
        'step_optimization': 15,
        'optimization': 50,
        'max_steps': 10
    }

    solver = Problem(**settings)
    solver.solve()
